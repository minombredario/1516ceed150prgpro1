/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import modelo.Medico;
import modelo.Persona;
import vista.VistaTerminal;

/**
 * Fichero: Vista_medico.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 20-oct-2015
 */
public class VistaMedico extends VistaPersona {

    public Medico altaMedico() throws IOException {
        String nombre;
        
        Medico medico = new Medico();
        Persona persona = new Persona();
        VistaPersona vp = new VistaPersona();
        VistaTerminal vt = new VistaTerminal();

        System.out.println("**   ALTA MEDICO   **");
        persona = vp.altaPersona();
        medico.setId(persona.getId());
        medico.setNombre(persona.getNombre());
        
       return medico;
    }

    public void muestraMedico(Medico medico) {
        System.out.println(medico);

    }
}

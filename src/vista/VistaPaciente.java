/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import modelo.Paciente;
import modelo.Persona;
import vista.VistaTerminal;

/**
 * Fichero: Vista_paciente.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 20-oct-2015
 */
public class VistaPaciente extends VistaPersona {

    public Paciente altaPaciente() throws IOException {
        String nombre;
        
        Paciente paciente = new Paciente();
        Persona persona = new Persona();

        VistaTerminal vt = new VistaTerminal();
        VistaPersona vp = new VistaPersona();

        System.out.println("**   ALTA PACIENTE   **");
        persona = vp.altaPersona();

        paciente.setId(persona.getId());
        paciente.setNombre(persona.getNombre());
        paciente.setObservaciones(persona.getObservaciones());
        
         

        return paciente;
    }

    public void muestraPaciente(Paciente paciente) {
        System.out.println(paciente);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import modelo.Cita;

/**
 * Fichero: Vista_cita.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 20-oct-2015
 */
public class VistaCita {

    public Cita pedirCita() throws IOException {

        VistaTerminal vt = new VistaTerminal();
        Cita cita = new Cita();

        int id;
        String hora;
        String dia;

        System.out.println("**   ALTA CITA   **");

        System.out.print("Id: ");
        id = vt.pedirInt();
        cita.setId(id);
        
        System.out.print("Hora: ");
        hora= vt.pedirString();
        cita.setHora(hora);
        
        System.out.print("Dia: ");
        dia= vt.pedirString();
        cita.setDia(dia);

        return cita;
    }

    public void muestraCita(Cita cita) {
        
        VistaMedico vm = new VistaMedico();
        VistaPaciente vp = new VistaPaciente();

        System.out.println("**************************");
        System.out.println("**   DATOS DE LA CITA   **");
        System.out.println("**************************");

        System.out.println(cita);
        System.out.println("Dia: " + cita.getDia() + "\tHora: " + cita.getHora());

        vm.muestraMedico(cita.getMedico());
        vp.muestraPaciente(cita.getPaciente());
        

    }
}

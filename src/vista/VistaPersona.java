package vista;

import java.io.IOException;
import modelo.Persona;

/**
 * Fichero: Vista_persona.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 14-oct-2015
 */
public class VistaPersona {

    private Object vt;

    public Persona altaPersona() throws IOException{
        int id;
        String nombre;
        int telefono;
        String observaciones;
        short edad;
        
        Persona persona = new Persona();
        VistaTerminal vt= new VistaTerminal();
        
        
        System.out.print("ID: ");
        id=vt.pedirInt();
        persona.setId(id);
              
        System.out.print("Nombre: ");
        nombre= vt.pedirString();
        persona.setNombre(nombre);
        
        System.out.print("Edad: ");
        edad= (short) vt.pedirInt();
        persona.setEdad(edad);
        
        System.out.print("Telefono: ");
        telefono= vt.pedirInt();
        persona.setTelefono(telefono);
        
        System.out.print("Observaciones: ");
        observaciones= vt.pedirString();
        persona.setObservaciones(observaciones);
        
        
        return persona;
    }

    public void mostrarPersona(Persona p) throws IOException {
        int id;
        System.out.println("Id: " + p.getId());

    }

}

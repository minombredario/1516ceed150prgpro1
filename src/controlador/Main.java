package controlador;

import java.io.IOException;
import modelo.Persona;
import modelo.Medico;
import modelo.Paciente;
import modelo.Cita;
import vista.VistaPaciente;
import vista.VistaMedico;
import vista.VistaCita;

/**
 * Fichero: main.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 13-oct-2015
 */
public class Main {

    public static void main(String args[]) throws IOException {
        /*Clase Main
         *Se encargara del control del programa
         *estara la funcion principal
         *Importara las clases de los
         paquetes de VISTA y MODELO
         */
        VistaPaciente vp;
        VistaMedico vm;
        VistaCita vc;
        Paciente paciente;
        Medico medico;
        Cita cita;

        //Creamos un objeto para cada modelo
        paciente = new Paciente();
        medico = new Medico();
        cita = new Cita();

        vp = new VistaPaciente();
        vm = new VistaMedico();
        vc = new VistaCita();

        paciente = vp.altaPaciente();
        medico = vm.altaMedico();

        //vp.muestraPaciente(paciente);
        //vm.muestraMedico(medico);
        cita = vc.pedirCita();

        cita.setMedico(medico);
        cita.setPaciente(paciente);

        vc.muestraCita(cita);

    }
}

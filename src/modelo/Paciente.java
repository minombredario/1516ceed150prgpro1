package modelo;

/**
 * Fichero: Paciente.java
 *
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @date 17-oct-2015
 */
public class Paciente extends Persona {

    private String nss;

    /**
     * @return the nss
     */
    public String getNss() {
        return nss;
    }

    /**
     * @param nss the nss to set
     */
    public void setNss(String nss) {
        this.nss = nss;
    }

    public String toString() {
        return "id: " + id + "\rNombre: " + nombre + "";
    }

}

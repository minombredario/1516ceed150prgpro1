package modelo;

/**
 * Fichero: Persona.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 13-oct-2015
 */
public class Medico extends Persona {

    private String ncolegiado;
    private String especialidad;

    /**
     * @return the ncolegiado
     */
    public String getNcolegiado() {
        return ncolegiado;
    }

    /**
     * @param ncolegiado the ncolegiado to set
     */
    public void setNcolegiado(String ncolegiado) {
        this.ncolegiado = ncolegiado;
    }

    /**
     * @return the especialidad
     */
    public String getEspecialidad() {
        return especialidad;
    }

    /**
     * @param especialidad the especialidad to set
     */
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String toString() {
        return id + "" + "\rMedico: " + nombre;
    }
}

package modelo;

/**
 * Fichero: Persona.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 13-oct-2015
 */
public class Persona {

    //private static final AtomicInteger count = new AtomicInteger(0);
    protected int id; // Permite acceder desde la subclase
    protected String nombre;
    protected short edad;
    protected int telefono;
    protected String observaciones;

    /**
     * @return the idpe
     */
    public int getId() {
        return id;
    }

    /**
     * @param idpe the idpe to set
     */
    public void setId(int idpe) {
        this.id = idpe;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the edad
     */
    public short getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(short edad) {
        this.edad = edad;
    }

    /**
     * @return the telefono
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String toString() {
        return id + "";
    }

}
